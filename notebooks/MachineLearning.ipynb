{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Zooniverse - Integrating Machine Learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zooniverse's _Caesar_ advanced retirement and aggregation engine allows for the setup of more advanced rules for retiring subjects. _Caesar_ also provides a powerful way of collecting and analysing volunteer classifications (aggregation). Machine learning models can be used with _Caesar_ to make classification workflows more efficient: in this notebook we will examine how machine learning can be used to implement advanced subject retirement rules and filter subjects prior to being shown to volunteers, as well as setting up an active learning cycle so that volunteer classifications help train the machine learning model.\n",
    "\n",
    "## Table of Contents\n",
    "\n",
    "1. [Setup](#Setup)\n",
    "2. [Advanced Retirement Using Machine Learning](#Advanced-Retirement-Using-Machine-Learning)\n",
    "    1. [Option 1: Make machine learning predictions for all subjects \"up front\"](#Option-1:-Make-machine-learning-predictions-for-all-subjects-\"up-front\")\n",
    "    2. [Option 2: Make machine learning predictions for each subject \"on the fly\"](#Option-2:-Make-machine-learning-predictions-for-each-subject-\"on-the-fly\")\n",
    "3. [Filtering Subjects Using Machine Learning](#Filtering-Subjects-Using-Machine-Learning)\n",
    "4. [Active Learning](#Active-Learning)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "You may need to install the `panoptes_client`, `pandas`, and `boto3` packages. If you do, then run the code in the next cell. Otherwise, skip it. `boto3` is a package that implements the Amazon Web Service (AWS) Python SDK (see the [documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html) for more detail), and is used in the second half of the tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!python -m pip install panoptes_client\n",
    "!python -m pip install pandas\n",
    "!python -m pip install boto3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from panoptes_client import Panoptes, Project, Subject, SubjectSet\n",
    "import pandas as pd\n",
    "import os\n",
    "import getpass\n",
    "import glob"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Advanced Retirement Using Machine Learning\n",
    "\n",
    "Trained machine learning models can take up a lot of disk space and can be a computationally intensive process. Using machine learning to predict classifications for specific subjects is not the sort of analysis that _Caesar_ can run by itself (even if you did add the code to the [`aggregation_for_caesar` repository](https://github.com/zooniverse/aggregation-for-caesar)). \n",
    "\n",
    "However, that doesn't mean that we can't use machine learning models and _Caesar_ **together** to make our classification workflow more efficient. For example, you might want to use a machine learning model to predict the class of every subject in a subject set and then use less stringent retirement criteria if the machine and human classifiers agree.\n",
    "\n",
    "As a concrete example, we might say that if the first three volunteers all agree with the machine prediction then we can retire the suject, but we would otherwise demand that seven volunteers view and classify the subject.\n",
    "\n",
    "There are a couple of easy ways to couple a machine learning model with your Zooniverse project and we'll quickly demo them here.\n",
    "\n",
    "1. The first option we have is to preclassify all of our subjects with machine learning and add the machine learning score to each as a **hidden subject metadatum** that _Caesar_ can recognise and use to execute specific retirement rules.\n",
    "2. The second option is to make machine learning predictions for subjects **after** they have been classified and make an \"on the fly\" retirement decision. The second approach is much more complicated, but it does mean that you can benefit from the latest version of your machine learning model and you can compute very complicated retirement criteria that are difficult or impossible to express as _Caesar_ rules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Option 1: Make machine learning predictions for all subjects \"up front\"\n",
    "\n",
    "If you have a trained machine learning model and a pool of data, then it's straightforward to predict a machine score for all of your subjects and add a new hidden metadatum to each one.\n",
    "\n",
    "For this tutorial, we have already trained a simple convolutional neural network (CNN) to predict the class (i.e. category) of a variable star's light curve by looking at the same images the volunteers see. We can hence load a table of its predictions for our subjects, where each row corresponds to a different subject and each column gives the predicted probability of that subject belonging to a given class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataDirectory = ?? # e.g. '../data/'\n",
    "predictions = pd.read_csv(os.path.join(dataDirectory, \"predictions.csv\")).set_index(\"image\")\n",
    "predictions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's create a new subject set (as we did in the _Advanced Project Building_ tutorial) and add the machine learning prediction to each subject as a hidden metadatum. We can call that metadatum whatever we like, so how about `#ml_prediction`? We'll simplify things by defining the machine prediction for a subject to be the class with the highest score, so for the zeroth row, our machine prediction is \"Pulsator\". Rather than storing the prediction as a word though, we'll map it to its corresponding position in the list of options for task 1 of our project. Recall that the order is:\n",
    "```python\n",
    "[\"Pulsator\", \"EA/EB type\", \"EW type\", \"Rotator\", \"Unkown\", \"Junk\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "questionOrder = [\"Pulsator\", \"EA/EB type\", \"EW type\", \"Rotator\", \"Unknown\", \"Junk\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start with all the parts we've seen before: authenticating with the Panoptes API and finding our project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "username = input(\"Enter Panoptes Username:\")\n",
    "password = getpass.getpass(\"Enter Panoptes Password:\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "panoptesClient = Panoptes.connect(username=username, password=password)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "projectId = ?? # You can look this up in the Project Builder\n",
    "project = Project.find(projectId)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Name:\", project.display_name)\n",
    "print(\"Description:\", project.description)\n",
    "print(\"Introduction:\", project.introduction)\n",
    "print(\"Number of Subjects:\", project.subjects_count)\n",
    "print(\"Number of Classifications:\", project.classifications_count)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's where we create our new subject set and give it a name and link it to our project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subjectSet = SubjectSet() # Define new subject set\n",
    "subjectSet.display_name = \"Machine Learning Demo Set\" # Give it a name\n",
    "subjectSet.links.project = project # Set it to link to our project\n",
    "response = subjectSet.save() # Save this (currently empty) subject set"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll acquire a file list of all the subjects to be added to the subject set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "imageFiles = glob.glob(os.path.join(dataDirectory, \"demoset_smallSample_1/*jpg\"))\n",
    "imageFiles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll add those files to the subject set along with some metadata. First, let's process the machine predictions like we discussed above, identifying the class with the highest score for each subject."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "imagePredictions = []\n",
    "for imageFile in imageFiles:\n",
    "    imageRow = int(os.path.basename(imageFile)[:-4])\n",
    "    imagePredictions.append(\n",
    "        (\n",
    "            imageRow, # Subject name/number\n",
    "            predictions.iloc[imageRow].idxmax(), # Class of variable star with the highest prediction score\n",
    "            questionOrder.index(predictions.iloc[imageRow].idxmax()), # Corresponding index\n",
    "        )\n",
    "    )\n",
    "imagePredictions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newSubjects = []\n",
    "\n",
    "for imageFile, imagePrediction in zip(imageFiles, imagePredictions):\n",
    "\n",
    "    newSubject = Subject()\n",
    "    newSubject.links.project = project\n",
    "    newSubject.add_location(imageFile)\n",
    "    newSubject.metadata = {\n",
    "        \"Origin\": \"Python ML demo\",\n",
    "        \"image\": os.path.basename(imageFile),\n",
    "        \"#ml_prediction\": imagePrediction[2],\n",
    "    }\n",
    "    newSubject.save()\n",
    "\n",
    "    newSubjects.append(newSubject)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finally, we assign our newly uploaded `Subject`s to the `SubjectSet` we already created."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subjectSet.add(newSubjects)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we just have to **set up appropriate retirement rules in the _Caesar_ UI**, shown [here](https://youtu.be/o9SzgsZvOCg?t=9003) (02:30:03-02:36:03) as part of the recorded demonstration of this tutorial. Zooniverse's _Caesar_ advanced retirement and aggregation engine allows for the setup of more advanced rules for retiring subjects and aggregating volunteer classifications. These involve the use of \"extractors\" and \"reducers\": extractors extract the data from each classification into a more useful data format, while reducers \"reduce\" (aggregate) the extracted data for each subject together.\n",
    "\n",
    "The input to a reducer is the output of one or more extractors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Option 2: Make machine learning predictions for each subject \"on the fly\"\n",
    "\n",
    "For this option, we want to make subject retirement decisions following machine learning predictions made _after_ volunteer classification. To do this, we'll need a way of outputting extract data for processing with machine learning, and a way for uploading the processed results back to _Caesar_ for the retirement decisions.\n",
    "\n",
    "#### External reducers\n",
    "While _Caesar_ contains multiple inbuilt reducers to choose from, _Caesar_ also defines the concept of an _External_ reducer. When you set up an external reducer in the _Caesar_ UI, the system will send the results from any associated extractors to an HTTP endpoint that you specify. You can then receive those extract data through the HTTP endpoint and process them appropriately.\n",
    "\n",
    "#### Placeholder reducers\n",
    "Once processing is complete, you need a way to send the results back to _Caesar_. The system defines the concept of a _Placeholder_ reducer for this purpose. Your external reducer should send its answer back to the placeholder reducer, allowing _Caesar_ to process any associated rules or effects.\n",
    "\n",
    "#### SQS reducers\n",
    "The one downside of a basic external reducer is that you need to run some sort of webserver that can listen out for messages from _Caesar_ and process them when they arrive. This can be a barrier to entry for some research teams, so _Caesar_ defines one more reducer category, the _SQS Reducer_. \n",
    "\n",
    "_SQS_ stands for \"Simple Queue Service\". It's a facility provided by Amazon Web Services that maintains a queue of messages in the cloud. _Caesar_ sends messages to this queue and your code can consume them. All the web hosting is handled by Amazon, so you don't have to worry.\n",
    "\n",
    "We'll **set up an SQS reducer in the _Caesar_ UI** to send message extracts to us, shown [here](https://youtu.be/o9SzgsZvOCg?t=9975) (02:46:15-02:56:36) as part of the recorded demonstration of this tutorial. Then we can use a specially written client to grab those messages from the queue and process them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ML_Demo import SQSClient"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**IMPORTANT**: Note that for you to use the SQS reducer facility you will need to set up an Amazon AWS account, set up a queue and get a set of access credentials. This process is free and reasonably straightforward, but beyond the scope of this tutorial. Once you've done that, you can export your credentials to your computer's environment like this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ[\"AWS_ACCESS_KEY_ID\"] = ?? # You will need an AWS account to get your ID\n",
    "os.environ[\"AWS_SECRET_ACCESS_KEY\"] = ?? # You will need an AWS account to get your Secret"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Every SQS queue has a unique URL that you can use to connect to it and retrieve messages. You must pass this URL to the `SQSClient` constructor when you instantiate the SQS client."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "queueUrl = ??  # You can set this up with an AWS account e.g. \"https://sqs.us-east-1.amazonaws.com/123456123456/MySQSQueue\"\n",
    "sqs = SQSClient(queueUrl)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The client provides a `getMessages` method that will retrieve a batch of messages from the queue and (optionally) delete them from the queue. The method returns three objects. The first is the set of messages that were received, with any duplicates removed, the second is the raw set of messages before deduplication and the final object is a list of unique message IDs for the messages that were received."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "messages, receivedMessages, receivedMessageIds = sqs.getMessages(delete=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can examine our messages more easily if we convert to a `pandas.DataFrame`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "messagesFrame = pd.DataFrame(messages)\n",
    "messagesFrame"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For every classification we get a row of corresponding data. The information about the volunteer's answer is in the `data` column. Since we set _Caesar_ to use a question extractor, the format of each entry can be interpreted as:\n",
    "```python\n",
    "{ '<answer>' : <number_of_matching_responses> } \n",
    "```\n",
    "For example, in row `0` of the table we can see that for subject `52665118` the classification contained one response for the answer `'0'`, which corresponds to a Pulsator.\n",
    "\n",
    "If we have a (partially) trained machine learning model, we could ask it to make its own predictions for each of the subjects we just got classifications for, but for now, let's just look up the classification (if it exists) in our table of predictions. We can use the `subject_id` field in our message table to get the metadata for each subject and then try to retrieve its machine learning prediction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "machineMatchesHuman = []\n",
    "for _, (subjectId, data) in messagesFrame[[\"subject_id\", \"data\"]].iterrows():\n",
    "    answer = next(iter(data.keys()))\n",
    "    if len(answer) == 0:\n",
    "        machineMatchesHuman.append(False)\n",
    "        continue\n",
    "    answer = int(answer)\n",
    "    try:\n",
    "        subject = Subject.find(subjectId)\n",
    "    except:\n",
    "        machineMatchesHuman.append(False)\n",
    "        continue\n",
    "    if \"image\" in subject.metadata:\n",
    "        lookup = int(subject.metadata[\"image\"][:-4])\n",
    "\n",
    "    if lookup in predictions.index:\n",
    "        print(\n",
    "            subjectId,\n",
    "            subject.metadata[\"image\"][:-4],\n",
    "            predictions.loc[lookup].idxmax(),\n",
    "            questionOrder[answer],\n",
    "        )\n",
    "        machineMatchesHuman.append(\n",
    "            answer == questionOrder.index(predictions.loc[lookup].idxmax())\n",
    "        )\n",
    "        continue\n",
    "    machineMatchesHuman.append(False)\n",
    "\n",
    "machineMatchesHuman"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perhaps we decide to retire all subjects for which the machine and human predictions match. In that case, we now need to send a message back to _Caesar_ with our decision for the subjects we want to retire. If we don't want to do anything, we don't have to send a message. \n",
    "\n",
    "We can use the Panoptes Python API to send a message back to the placeholder reducer we set up in the Caesar UI. Here's how we do it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pan = Panoptes(login=\"interactive\", redirect_url='https://caesar.zooniverse.org/auth/zooniverse/callback')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wokflowId = ?? # You can look this up in the Project Builder or you could use: project.links.workflows[0]\n",
    "response = pan.put(endpoint=\"https://caesar.zooniverse.org\", \n",
    "                   path=f\"workflows/{wokflowId}/subject_reductions/receiver\", \n",
    "                   json={\n",
    "                       'reduction': {\n",
    "                           'subject_id': int(messagesFrame.subject_id.iloc[-1]),\n",
    "                           'data': {\"retire\": True}\n",
    "                       }\n",
    "                   })"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the reduction was successfully submitted, _Caesar_ will send a `dict` in response."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "response"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Success!\n",
    "\n",
    "#### Aside: Avoid entering credentials\n",
    "To avoid having to enter credentials to send reductions back to _Caesar_, you can register with the Panoptes authentication system and get a _Client ID_ and a _Client Secret_. These are just special strings of unguessable characters that the Panoptes Python API can use instead of your usual credentials to authenticate.\n",
    "\n",
    "To get your _Client ID_ and _Client Secret_ visit [https://panoptes.zooniverse.org/oauth/applications](https://panoptes.zooniverse.org/oauth/applications) and click on _New Application_. Once you have those details you can export them to your computer's environment just like you did for the Amazon credentials, but with different names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ[\"PANOPTES_CLIENT_ID\"] = ??\n",
    "os.environ[\"PANOPTES_CLIENT_SECRET\"] = ??"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filtering Subjects Using Machine Learning\n",
    "\n",
    "Although machine learning algorithms are very good at performing **specific tasks**, there are some things that human beings still tend to do better at. For example, human beings are much more likely to spot unusual or unexpected features in images or other types of subjects.\n",
    "\n",
    "However, there are many data sets (with more arriving every day) that are simply too large to be processed by human beings, even using a citizen science approach.\n",
    "\n",
    "Machine learning can help here by filtering out subjects that are \"not interesting\". Such subjects are typically very common in the data sets that were used to train the machine learning models and are therefore very easily and confidently classified using those models.\n",
    "\n",
    "Commonly cited examples of how machine learning has been used to filter subjects that are shown to volunteers on Zooniverse are ecology-focused \"camera trap\" projects. Volunteers are asked to identify any animals they see in the images they are shown. Machine learning models detect \"empty\" images very accurately and it is not useful for volunteers to classify images with no animals in them. Machine learning can be used very effectively to remove empty images from Zooniverse subject sets to let volunteers focus on classifying animals.\n",
    "\n",
    "We'll use an example from our _SuperWASP Variable Stars_ CNN, in which we'll select only those light curves for which our model is \"confused\". We'll define \"confusing\" images as those for which the machine learning algorithm outputs a response greater than 0.4 for more than one category.\n",
    "\n",
    "For this demonstration, we've preselected those subjects and you can find the images in the `data/demoset_confused` folder. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "confusedPredictions = pd.read_csv(os.path.join(dataDirectory, \"confused_predictions.csv\")).set_index(\"image\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "confusedPredictions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make a quick plot of some of these \"confusing\" images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ML_Demo import plotConfusedBatch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plotConfusedBatch(confusedPredictions.iloc[:, :6], os.path.join(dataDirectory, \"demoset_confused\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I think we can agree that the answers certainly aren't obvious, but as humans (maybe experts) we can probably get all of them right. Let's use our normal techniques to make a new \"confused\" subject set and add our confused subjects to it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subjectSet = SubjectSet()\n",
    "subjectSet.display_name = \"Confused Demo Set\"\n",
    "subjectSet.links.project = project\n",
    "response = subjectSet.save()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "newSubjects = []\n",
    "\n",
    "for image, imagePrediction in confusedPredictions.iloc[:, :6].iterrows():\n",
    "\n",
    "    newSubject = Subject()\n",
    "    newSubject.links.project = project\n",
    "    newSubject.add_location(os.path.join(os.path.join(dataDirectory, \"demoset_confused\"), f\"{image}.jpg\"))\n",
    "    newSubject.metadata = {\n",
    "        \"Origin\": \"Python ML demo\",\n",
    "        \"image\": f\"{image}.jpg\",\n",
    "        \"ml_prediction\": dict(imagePrediction),\n",
    "    }\n",
    "    newSubject.save()\n",
    "\n",
    "    newSubjects.append(newSubject)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subjectSet.add(newSubjects)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Active Learning\n",
    "\n",
    "We're now in a position to implement a toy demonstration of a technique called \"active learning\". In active learning, model predictions are used to select subjects that are likely to provide the most useful information to improve the model's performance if they were labelled and used for further training the model. This is particularly useful when your available data set is largely unlabelled.\n",
    "\n",
    "For example, if you have a method of obtaining a level of confidence in a prediction made by your model, such as the uncertainty value predicted by a Bayesian neural network alongside each class prediction, then subjects with the highest predicted uncertainties would likely be the most useful for active learning. \n",
    "\n",
    "Our \"confusing subjects\" selection probably isn't exactly the right approach, but it's a reasonable attempt.\n",
    "\n",
    "Now that we've created a new subject set with confusing images, let's create a special \"Advanced\" workflow to process them. Like before, we can use an external reducer on _Caesar_ to send classifications from that workflow to our SQS queue once they're classified by volunteers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "messages, receivedMessages, receivedMessageIds = sqs.getMessages(delete=True)\n",
    "messagesFrame = pd.DataFrame(messages)\n",
    "messagesFrame"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we collect enough classifications from our \"Advanced\" workflow, we can use those classifications to further train our model. We can then use this model to make predictions for more subjects, select a new set of the most confusing subjects, and use them to create another subject set for the \"Advanced\" workflow.\n",
    "\n",
    "Repeating this cycle over and over again is the basis of active learning with Zooniverse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
